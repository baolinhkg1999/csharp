﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem02
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Tạo array mẫu
            int[] array = { 1, 2, 5, -4, 3 }; //Nếu subLength = 3 thì sẽ có các mảng con {1, 2, 5} {2, 5, -4} {5, -4, 3}
            Console.WriteLine(FindMaxSubArray(array, 5));
            Console.ReadKey();
        }

        static public int FindMaxSubArray(int[] inputArray, int subLength)
        {
            int sum = inputArray[0];
            int curr_max = inputArray[0];

            for (int i = 1; i < subLength; i++)
            {
                curr_max = Math.Max(inputArray[i], curr_max + inputArray[i]);
                sum = curr_max;
            }

            return sum;
        }
    }
}
