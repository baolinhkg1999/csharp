﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NPL.Practice.T02.Problem01
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(GetArticleSummary("One of the world's biggest festivals hit the streets of London", 50));
        }

        static public string GetArticleSummary(string content, int maxLength)
        {
            //Tạo finalString để lưu trữ chuỗi sau khi xử lí.
            string finalString;

            //Xử lý ngoại lệ nếu maxLength lớn hơn số lượng từ trong chuỗi.
            if (maxLength > content.Length)
                return "ERROR: maxLength cannot larger than length of content.";
            else
            {   
                finalString = content.Substring(0, maxLength); //Cắt chuỗi từ vị trí đầu tiên (0) đến maxLength
                finalString = finalString + "..."; //Thêm "..." vào cuối chuỗi.

                return finalString;
            }    
        }
    }
}
